package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class UserByLoginNotFoundException extends AbstractException {

    public UserByLoginNotFoundException(final String message) {
        super("Error! User with LOGIN: " + message + " not found");
    }

}
