package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class UserByEmailNotFoundException extends AbstractException {

    public UserByEmailNotFoundException(final String message) {
        super("Error! User with EMAIL: " + message + " not found");
    }

}
