package ru.goloshchapov.tm.exception.system;

import ru.goloshchapov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String value) {
        super("Error! Undetermined argument " + value + " ...");
    }

}
