package ru.goloshchapov.tm.exception.system;

import ru.goloshchapov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String value) {
        super("Error! Command " + value + " is not found...");
    }

}
