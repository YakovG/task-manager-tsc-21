package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.addAll(collection);
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E findOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findOneById(id);
    }

    @Override
    public E findOneByIndex(final Integer index) {
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public boolean isAbsentById(final String id) {
        if (isEmpty(id)) return true;
        return repository.isAbsentById(id);
    }

    @Override
    public boolean isAbsentByIndex(final Integer index) {
        if (!checkIndex(index, size())) return true;
        return repository.isAbsentByIndex(index);
    }

    @Override
    public String getIdByIndex(final Integer index) {
        if (!checkIndex(index, size())) throw new IndexIncorrectException();
        return repository.getIdByIndex(index);
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public E removeOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.removeOneById(id);
    }

    @Override
    public E removeOneByIndex(final Integer index) {
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }
}
