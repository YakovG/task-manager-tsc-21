package ru.goloshchapov.tm.model;

public final class Task extends AbstractBusinessEntity {

    private String projectId = null;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}