package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void addAll(Collection<E> collection);

    E add(E entity);

    List<E> findAll();

    E findOneById(String id);

    E findOneByIndex(Integer index);

    boolean isAbsentById(String id);

    boolean isAbsentByIndex(Integer index);

    String getIdByIndex(Integer index);

    int size();

    void clear();

    void remove(E entity);

    E removeOneById(String id);

    E removeOneByIndex(Integer index);

}
