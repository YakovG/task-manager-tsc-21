package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
