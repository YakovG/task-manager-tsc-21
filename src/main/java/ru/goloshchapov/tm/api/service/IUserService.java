package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

public interface IUserService extends IService<User> {

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password, String email, String role);

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User removeUser(User user);

    User removeUserByLogin(String login);

    User setPassword(String userId, String password);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );
}
