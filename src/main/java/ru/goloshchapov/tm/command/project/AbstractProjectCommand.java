package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("CREATED: " + project.getCreated());
        if (project.getDateStart() != null) System.out.println("STARTED: " + project.getDateStart());
        if (project.getDateFinish() != null) System.out.println("FINISHED: " + project.getDateFinish());
    }

}
